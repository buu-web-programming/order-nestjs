import { IsNotEmpty, MinLength, IsInt, IsPositive } from 'class-validator';

export class CreateProductDto {
  @IsNotEmpty()
  name: string;

  @IsNotEmpty()
  @IsInt()
  @IsPositive()
  price: number;
}
